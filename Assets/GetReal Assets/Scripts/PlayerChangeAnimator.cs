﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChangeAnimator : MonoBehaviour {

	public int currentID = 0;
	public AnimatorOverrideController[] animatorOverrider;
	private Animator animator;

	private bool change = false;

	void Awake () {
		animator = GetComponent<Animator> ();
	}

	public void AlertObservers(string message) {

		if (message.Equals ("ChangeAnimationEnd") && change) {
			


			animator.runtimeAnimatorController = animatorOverrider [currentID];
			currentID++;
			if (currentID > animatorOverrider.Length-1)
				currentID = 0;
			change = false;
			animator.SetBool("Change", change);
		}
	}

	public void ChangeToNextAnimator() {
		change = true;
		animator.SetBool("Change", change);
	}
}
// check AnimationOverrideController
