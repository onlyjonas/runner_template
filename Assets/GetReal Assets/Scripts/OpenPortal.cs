﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OpenPortal : MonoBehaviour {

	public GameObject portalKey;
	public UnityEvent portalEvent;

	void OnTriggerEnter2D( Collider2D other ) {

		if (other.gameObject.tag == "Player") {

			GameObject playerObj = other.gameObject.GetComponent<PlayerCollectObjects> ().collectedObject;

			if ( playerObj == portalKey) {


				playerObj.SetActive (true);
				other.gameObject.GetComponent<PlayerCollectObjects> ().collectedObject = null;
				portalEvent.Invoke() ;


			}

		}
	}
}
