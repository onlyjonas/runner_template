﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_DestroyObject : MonoBehaviour {

	void OnTriggerEnter2D( Collider2D other ) {
		Destroy (other.gameObject);	
	}

}
