﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollectObjects : MonoBehaviour {

	public GameObject collectedObject;

	public void CollectObject(GameObject collObj) {
		collectedObject = collObj;
	}

	public GameObject GetCollectedObject() {
		return collectedObject;
	}
}
