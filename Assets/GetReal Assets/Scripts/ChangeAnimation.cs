﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAnimation : MonoBehaviour {

	public int animationParameterID = 0;
	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator> ();
	}

	public void ChangeAnimationID (int id) {
		animator.SetInteger ("AnimationID", id);
	}
}
