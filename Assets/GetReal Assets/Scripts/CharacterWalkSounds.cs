﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets._2D
{
public class CharacterWalkSounds : MonoBehaviour {
	
	public AudioClip[] walkSounds = new AudioClip[3];

	public float minSpeed = 0.3f;
	public float walkStepLength = 0.2f;
	public float runStepLength = 0.2f;

	private PlatformerCharacter2D character;
	private float playerSpeed;
	private Animator m_Anim;

	void Awake()
	{
		character = GetComponent<PlatformerCharacter2D>();
		m_Anim = GetComponent<Animator>();

	}
	

	// Use this for initialization
	void Update () {
		StartCoroutine(PlayWalkSound ());
	}

	private bool isPlaying = false;

	IEnumerator PlayWalkSound() {


			if (character.m_Grounded && GetComponent<Rigidbody2D>().velocity.magnitude > minSpeed) {

			if(!isPlaying) {

				isPlaying = true;
					
				//Spawn a new object with AudioSource and Clip set, at a point in the 3D world
				AudioSource.PlayClipAtPoint (walkSounds [Random.Range (0, walkSounds.Length)], transform.position);
		
					float delay;
					if (m_Anim.GetBool ("Run"))
						delay = runStepLength;
					else
						delay = walkStepLength;
				yield return new WaitForSeconds (delay);

				isPlaying = false;
					
					
			}

		}
	
	}
		
}
}

