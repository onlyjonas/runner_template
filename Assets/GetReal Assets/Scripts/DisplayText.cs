﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayText : MonoBehaviour {

	public Transform textMeshPrefab;
	public Vector2 offsetPosition;
	public float textDisplayTime = 2f;
	public string[] texte;

	private Transform textMesh;
	private int textIndex = 0; 
	private float myTimer;

	void Awake() {
		if(!textMeshPrefab) textMeshPrefab = Resources.Load("TextMeshPrefab") as Transform;
	}

	void Start () {
		textMesh = Instantiate (textMeshPrefab, new Vector3(transform.position.x + offsetPosition.x,transform.position.y + offsetPosition.y,transform.position.z), Quaternion.identity);						
		textMesh.parent = transform;
		textMesh.GetComponent<MeshRenderer> ().sortingLayerName = GetComponent<SpriteRenderer> ().sortingLayerName;
		textMesh.GetComponent<MeshRenderer> ().sortingLayerID = GetComponent<SpriteRenderer> ().sortingLayerID;
	}

	void Update () {
		if (textMesh.GetComponent<TextMesh>().text != null) {

			if(myTimer > 0) myTimer -= Time.deltaTime;
			else textMesh.GetComponent<TextMesh>().text = null;

		}
	}

	public void DispayText(string text) {
		textMesh.GetComponent<TextMesh>().text = text;
		myTimer = textDisplayTime;
	}

	public void DispayNextText() {
		textMesh.GetComponent<TextMesh>().text = texte[textIndex];
		myTimer = textDisplayTime;

		if (textIndex < texte.Length - 1)
			textIndex++;
		else
			textIndex = 0;
	}
}
