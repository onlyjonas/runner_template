﻿using UnityEngine;
using UnityEngine.Events;

public class Event_CollisionEnter2D : MonoBehaviour {

	public string objectTag;
	public UnityEvent collisionEnterEvent;

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == objectTag ) {
			collisionEnterEvent.Invoke ();
		}
	}

}
