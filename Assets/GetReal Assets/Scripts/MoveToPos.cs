﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPos : MonoBehaviour {

	public Vector2 moveToPos;
	public float speed = 1.0f;
	public bool active = false;

	private Vector2 defaultPos;
	private Vector2 targetPos;


	void Start () {
		defaultPos = transform.position;
		targetPos = moveToPos;
	}
	
	void Update () {

		if (active == true) {

			float step = speed * Time.deltaTime;

			transform.position = Vector2.MoveTowards(transform.position, targetPos, step);

			if(Vector2.Distance(transform.position, moveToPos) == 0) targetPos = defaultPos;
			else if(Vector2.Distance(transform.position, defaultPos) == 0) targetPos = moveToPos;
		}

	}

	public void StartMoving() {
		active = true;	
	}

	public void StopMoving() {
		active = true;	
	}

	public void ChangeDirection() {
		if(targetPos == moveToPos) targetPos = defaultPos;
		else targetPos = moveToPos;
	}
}
