﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapPlayerAround : MonoBehaviour {

	public float minX = -12.5f;
	public float maxX = 12.5f;
	public float minY = -10f;
	public float maxY = 10f;

	public Vector2 maxVelocity;

	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {

		// Horizontal Wrap around X

		if (transform.position.x > maxX) {

			Vector3 newPosition = new Vector3 (minX, transform.position.y, transform.position.z );
			transform.position = newPosition;

		}

		if (transform.position.x < minX) {

			Vector3 newPosition = new Vector3 (maxX, transform.position.y, transform.position.z );
			transform.position = newPosition;

		}

		// Vertical Wrap around Y

		if (transform.position.y > maxY) {

			Vector3 newPosition = new Vector3 (transform.position.x, minY, transform.position.z );
			transform.position = newPosition;

		}

		if (transform.position.y < minY) {

			Vector3 newPosition = new Vector3 (transform.position.x, maxY, transform.position.z );
			transform.position = newPosition;

			// check velocity
			if (rb.velocity.x  > maxVelocity.x) {
				rb.velocity = new Vector2(maxVelocity.x, rb.velocity.y );
			}
			if (rb.velocity.x  < -maxVelocity.x) {
				rb.velocity = new Vector2(-maxVelocity.x, rb.velocity.y );
			}

			if (rb.velocity.y > maxVelocity.y) {
				rb.velocity = new Vector2(rb.velocity.x , maxVelocity.y  );
			}

			if (rb.velocity.y < -maxVelocity.y) {
				rb.velocity = new Vector2(rb.velocity.x , -maxVelocity.y  );
			}


		}

	}
}
