﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamTarget_OnlyMoveRight : MonoBehaviour {

	public Transform target;
	private float yOffset;

	void Start () {
		yOffset = transform.position.y - target.position.y;
	}
	

	void Update () {
	
		if (target.position.x > transform.position.x) {
			Vector3 newPos = new Vector3 (target.position.x, target.position.y + yOffset, transform.position.z); 
			transform.position = newPos;
		}

	}
}
