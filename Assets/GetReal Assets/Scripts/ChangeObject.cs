﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeObject : MonoBehaviour {

	public Transform changeToPrefab;

	public void ChangeToPrefab () {

		Debug.Log ("CHANGE");

		Transform obj = Instantiate(changeToPrefab, transform.position, Quaternion.identity);
		obj.parent = transform.parent;
		Destroy(gameObject);
	
	}

}
