﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByKeys : MonoBehaviour {

	public float speed = 5.0f;
	private Vector3 moveDirection;

	void Update () {
		moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
		moveDirection *= speed;
		transform.Translate (moveDirection * Time.deltaTime, Space.Self);
	}
}
