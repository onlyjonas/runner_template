﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {

	public AudioClip clip;
	private AudioSource audio;
 
	void Start () {
	    audio = GetComponent<AudioSource>();
	}
 
	public void playMySound() {
		AudioSource.PlayClipAtPoint (clip, Camera.main.transform.position, 1f);
	}
}


