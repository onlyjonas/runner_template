﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpThroughPlatform : MonoBehaviour {

	public Transform player;
	public float playerSize = 2f;
	private BoxCollider2D myCol;


	// Use this for initialization
	void Start () {
		myCol = GetComponent<BoxCollider2D> ();
		}

	// Update is called once per frame
	void Update () {

		if (player.position.y - playerSize/2 > transform.position.y + myCol.size.y/2 ) {
			myCol.enabled = true;
		} else {
			myCol.enabled = false;
		}


	}
}
