using UnityEngine;
using System.Collections;

public class Camera2DFollow : MonoBehaviour {

	public float cameraZoomFactor = 5;
	public Transform target;
	public float damping = 1;
	public float lookAheadFactor = 3;
	public float lookAheadReturnSpeed = 0.5f;
	public float lookAheadMoveThreshold = 0.1f;
	
	private float offsetZ;
	private Vector3 lastTargetPosition;
	private Vector3 currentVelocity;
	private Vector3 lookAheadPos;

	private Transform myTarget;
	private float newZoom;
	private float zoomVelocity;

	void Start () {
		lastTargetPosition = target.position;
		offsetZ = (transform.position - target.position).z;
		transform.parent = null;
		myTarget = target;
		newZoom = cameraZoomFactor;
	}

	void Update () {
		
		// only update lookahead pos if accelerating or changed direction
		float xMoveDelta = (myTarget.position - lastTargetPosition).x;

	    bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

		if (updateLookAheadTarget) {
			lookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
		} else {
			lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);	
		}
		
		Vector3 aheadTargetPos = myTarget.position + lookAheadPos + Vector3.forward * offsetZ;
		Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);
		
		transform.position = newPos;
		lastTargetPosition = myTarget.position;		
	
		float myZoom = Mathf.SmoothDamp(GetComponent<Camera> ().orthographicSize, newZoom , ref zoomVelocity, damping);
		GetComponent<Camera> ().orthographicSize = myZoom;
		
	
	}

	public void CameraSwitchTarget(Transform newTarget) {
		myTarget = newTarget;
	}

	public void CameraSetZoom(float zoom) {
		newZoom = zoom;
	}


}
