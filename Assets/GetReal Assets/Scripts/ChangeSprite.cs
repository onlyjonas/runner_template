﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : MonoBehaviour {

	private Sprite defaultSprite;

	void Start () {
		defaultSprite = GetComponent<SpriteRenderer> ().sprite;
	}

	public void ChangeMySprite (Sprite newSprite) {
		GetComponent<SpriteRenderer> ().sprite = newSprite;
	}


}
