﻿using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof (Rigidbody2D))]

public class SwimmerCharacter2D : MonoBehaviour {

	public UnityEvent actionButtonDownEvent;
	public UnityEvent actionButtonUpEvent;

	public bool floating;
	public float floatingStrength = 0.2f;
	public float speed = 0.2f;
	public float moveSpeed = 5.0f;
	public float rotationSpeed = 5.0f;
	public float rotationUpSpeed = 5.0f;

	private Vector3 moveDirection;
	private float rotation;
	private Rigidbody2D m_Rigidbody2D;
	private Animator m_Anim;
	private Vector3 defaultScale;
	private float swimFaster = 1f; 

	// Todo:
	// chrouch?
	// Ceiling / Ground check?

	// Use this for initialization
	void Start () {
		m_Anim = GetComponent<Animator>();
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		defaultScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if (floating) {
			FloatInWater (floatingStrength);
		}

		// Move
		float x = CrossPlatformInputManager.GetAxis("Horizontal") * moveSpeed * swimFaster;
		float y = CrossPlatformInputManager.GetAxis ("Vertical") * moveSpeed * swimFaster;
		Vector2 moveDir = new Vector2(x,y);
		float swimSpeed = (Mathf.Abs(x) + Mathf.Abs(y))/2;

		m_Rigidbody2D.AddForce(moveDir)	;
		m_Anim.SetFloat("Speed", swimSpeed);

		// Rotation
		Vector3 newDir = new Vector3 (x, y, 0);
		Debug.DrawRay(transform.position, newDir, Color.red);
		Quaternion targetRotation = Quaternion.FromToRotation (Vector3.up, newDir);
		// only rotate around Z Axes
		targetRotation.eulerAngles = new Vector3(0,0,targetRotation.eulerAngles.z);

		transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, Time.deltaTime * rotationUpSpeed);


		if (swimSpeed == 0) {
			floating = true;
		} else {
			floating = false;
		}


		// flip sprite depending on swim direction
		if (!floating) {
			if (x > 0)
				transform.localScale = new Vector3 (-defaultScale.x, defaultScale.y, defaultScale.z);
			if (x < 0)
				transform.localScale = new Vector3 (defaultScale.x, defaultScale.y, defaultScale.z);
		}

	}

	private void FixedUpdate()
	{
		if (CrossPlatformInputManager.GetButtonDown ("Fire1")) {
			swimFaster = 2f;
		} 
		if (CrossPlatformInputManager.GetButtonUp ("Fire1")) {
			swimFaster = 1f;
		}



		if(CrossPlatformInputManager.GetButtonDown("Fire2")) {
			actionButtonDownEvent.Invoke();
		}
		if(CrossPlatformInputManager.GetButtonUp("Fire2")) {
			actionButtonUpEvent.Invoke();
		}

	}

	public void FloatInWater(float strength)
	{
	    float scale = Mathf.PingPong(Time.time*speed,  strength) - strength/2;
		GetComponent<Rigidbody2D> ().gravityScale = scale;
	}
}
