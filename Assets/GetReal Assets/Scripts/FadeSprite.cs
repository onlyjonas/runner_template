using UnityEngine;
using System.Collections;
 
public class FadeSprite : MonoBehaviour {
 
	private float fadeSpeed;
	private float alpha;
	private float prevAlpha;
	private int fadeDir;
	private SpriteRenderer rend;
	private bool active = false;
	 
	void Start () {
		rend = gameObject.GetComponent<SpriteRenderer> ();
		alpha = rend.material.color.a;
	}
	 
	public void In (float speed) {
		fadeDir = 1;
		fadeSpeed = speed;
		active = true;
	}
	 
	public void Out (float speed) {
		fadeDir = -1;
		fadeSpeed = speed;
		active = true;
	}
	 
	void Update() {
		 
		if (active) {
		 
			alpha += fadeDir * fadeSpeed * Time.deltaTime;
			alpha = Mathf.Clamp01 (alpha);
			 
			Color c = rend.material.color;
			Color newCol = new Color (c.r, c.g, c.b, alpha);
			rend.material.color = newCol;
			 
			if (prevAlpha == alpha) {
				active = false;
			} else {
				prevAlpha = alpha;	
			}
		}
	}
}