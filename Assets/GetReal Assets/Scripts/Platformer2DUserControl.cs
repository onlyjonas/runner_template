using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Events;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
		public UnityEvent actionButtonDownEvent;
		public UnityEvent actionButtonUpEvent;
		public UnityEvent upButtonDownEvent;
		public UnityEvent downButtonDownEvent;

		private PlatformerCharacter2D m_Character;
        private bool m_Jump;


        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

        }

		bool upOnce = false;
		bool downOnce = false;

        private void FixedUpdate()
        {
            // Read the inputs.
			bool crouch = CrossPlatformInputManager.GetButton("Fire1");
			bool run = CrossPlatformInputManager.GetButton("Fire3");

			if(CrossPlatformInputManager.GetButtonDown("Fire2")) {
				actionButtonDownEvent.Invoke();
			}
			if(CrossPlatformInputManager.GetButtonUp("Fire2")) {
				actionButtonUpEvent.Invoke();
			}

			float v = CrossPlatformInputManager.GetAxis("Vertical");
			if(v > 0 && !upOnce) {
				upButtonDownEvent.Invoke();
				upOnce = true;
			}
			if(v < 0 && !downOnce) {
				downButtonDownEvent.Invoke();
				downOnce = true;
			}
			if(v == 0) {
				upOnce  = false;
				downOnce = false;
			}


            float h = CrossPlatformInputManager.GetAxis("Horizontal");


            // Pass all parameters to the character control script.
			m_Character.Move(h, run, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
