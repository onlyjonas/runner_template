﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MoviePlayer : MonoBehaviour {

	// Control movie speed with object speed (velocity)

	public Transform Object;
	private VideoPlayer videoPlayer;
	public float speed;

	void Start() {
		videoPlayer = GetComponent<VideoPlayer> ();
	}

	void Update()
	{

		//Debug.Log(Player.GetComponent<Rigidbody2D>().velocity);
		speed = Object.GetComponent<Rigidbody2D> ().velocity.x;

		videoPlayer.playbackSpeed = Mathf.Clamp(speed, 0, 1f);
	}
}
