﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public void LoadScene(int id) {
		SceneManager.LoadScene(id);
	}
}
