﻿using UnityEngine;
using System.Collections;

// Texture Scroller can only used on 3D Objects not Sprites
// therefor put your Sprite on a 2D Plane with a Material (Unlit/Transparent)

[RequireComponent(typeof (MeshRenderer))]
public class TextureOffsetScroller : MonoBehaviour {

	public Vector2 scrollSpeed;
	private Vector2 defaultOffset;
	public bool active = true;

	void Start () {
		defaultOffset = GetComponent<MeshRenderer> ().material.GetTextureOffset ("_MainTex");
	}

	void Update () {
		if (active) {
			float x = Mathf.Repeat (Time.time * scrollSpeed.x, 1);
			float y = Mathf.Repeat (Time.time * scrollSpeed.y, 1);
			Vector2 offset = new Vector2 (defaultOffset.x + x, defaultOffset.y + y);
			GetComponent<MeshRenderer> ().material.SetTextureOffset ("_MainTex", offset);
		}
	}

	public void activateScroller (bool a) {
		active = a;
	}
}