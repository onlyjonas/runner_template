﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeRestartGame : MonoBehaviour {

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}

	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Debug.Log (">>>>> QUIT GAME");
			Application.Quit();
		}

		if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) ) {
			if (Input.GetKeyDown ("r")) {
				Debug.Log (">>>>> RESTART GAME");
				SceneManager.LoadScene (0);
				Destroy (gameObject);
			}
		}

	}
}
