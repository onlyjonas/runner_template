﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMe : MonoBehaviour {

	public float rotationSpeed = 100f;
	public bool active = false;

	void Update () {
		if (active) {
			transform.Rotate (0, 0, rotationSpeed * Time.deltaTime);
		} 
	}

	public void StartRotation() {
		active = true;
	}

	public void StopRotation () {
		active = false;
	}

	public void ChangeDirection() {
		rotationSpeed = -rotationSpeed;
	}
}
