﻿using UnityEngine;
using UnityEngine.Events;

public class Event_PlayerTriggerEnter2D : MonoBehaviour {

	public UnityEvent triggerEnterEvent;
	public UnityEvent triggerExitEvent;

	void OnTriggerEnter2D( Collider2D other ) {
		if (other.gameObject.tag == "Player" && other.GetType ().Name == "BoxCollider2D"  ) {
			Debug.Log ("Player enter Trigger " + gameObject.name);
			triggerEnterEvent.Invoke ();
		}
	}

	void OnTriggerExit2D( Collider2D other ) {
		if (other.gameObject.tag == "Player" && other.GetType ().Name == "BoxCollider2D"  ) {
			Debug.Log ("Player exit Trigger " + gameObject.name);
			triggerExitEvent.Invoke ();
		}
	}
}
