﻿using System.Collections;
using UnityEngine.Events;
using UnityEngine;

public class Event_TimerEvent: MonoBehaviour {

	public bool StartTimerOnStart = true;
	public float StartTimerDuration = 5f;

	public UnityEvent EventOnTimerStart;
	public UnityEvent EventOnTimerEnd;

	// Use this for initialization
	void Start () {
		if(StartTimerOnStart) StartCoroutine(MyTimer(StartTimerDuration));
	}

	public void StartMyTimer(float duration) {
		StartCoroutine(MyTimer(duration));
	}

	IEnumerator MyTimer(float duration) {

		Debug.Log (gameObject.name + ": start timer event");
		EventOnTimerStart.Invoke ();

		yield return new WaitForSeconds (duration);

		Debug.Log (gameObject.name + ": end timer event");
		EventOnTimerEnd.Invoke ();

	}
}
