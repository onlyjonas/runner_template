﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody2D))]

public class Floating : MonoBehaviour {

	public bool floating;
	public float floatingStrength = 0.2f;
	public float speed = 0.2f;
	public float maxFloatDistance = 1f;

	private float defaultGravityScale;
	private Vector3 defaultPosition;
	private bool floatActive = true;

	// Use this for initialization
	void Start () {
		defaultGravityScale = GetComponent<Rigidbody2D> ().gravityScale;
		defaultPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (floating) {
			FloatInWater (floatingStrength);
		} else {
			GetComponent<Rigidbody2D> ().gravityScale = defaultGravityScale;
		}
	}


	void FloatInWater(float strength)
	{
		

		if (Vector3.Distance (defaultPosition, transform.position) < maxFloatDistance && floatActive) {
			float scale = Mathf.PingPong (Time.time * speed, strength) - strength / 2;
			GetComponent<Rigidbody2D> ().gravityScale = scale;
		} else {
			floatActive = false;

			if (Vector3.Distance (defaultPosition, transform.position) < 0.1f) {
				floatActive = true;
			}
			GetComponent<Rigidbody2D> ().gravityScale = 0f;
			transform.position = Vector3.MoveTowards( transform.position, defaultPosition, Time.deltaTime);

		}
			
	}

	public void ActivateFloating (bool active) {
		floating = active;
		defaultPosition = transform.position;
	}
}
