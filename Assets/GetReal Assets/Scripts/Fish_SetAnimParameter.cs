﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish_SetAnimParameter : MonoBehaviour {

	public void SetAnimParameter(bool active) {
		Animator m_Anim = GetComponent<Animator>();
		m_Anim.SetBool("dead", active);
	}
}
