﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnMovingPlatform : MonoBehaviour {

	void OnTriggerStay2D (Collider2D other) {

		other.transform.parent = transform;

	}

	void OnTriggerExit2D (Collider2D other) {

		other.transform.parent = null;

	}

}
