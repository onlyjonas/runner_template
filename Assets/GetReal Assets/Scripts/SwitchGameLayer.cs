﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGameLayer : MonoBehaviour {

	public int currentLayer = 0; 
	public GameObject[] allLayers;
	public float offsetYPos = 0.05f;

	public float fadeSpeed = 2f;

	public Color activeColor;
	public Color nonActiveColor;


	// Use this for initialization
	void Start () {
		ActivateLayer (currentLayer);

	}

	// Update is called once per frame
	void Update () {

		for (int i = 0; i < allLayers.Length; i++) {
			SpriteRenderer[] renderer = allLayers[i].GetComponentsInChildren<SpriteRenderer> ();
			if (i == currentLayer) {
				foreach (SpriteRenderer r in renderer) {
					r.color = Color.Lerp (r.color, activeColor, Time.deltaTime * fadeSpeed);
				}
			} else {
				foreach (SpriteRenderer r in renderer) {
					r.color = Color.Lerp (r.color, nonActiveColor, Time.deltaTime * fadeSpeed);
				}
			}
		}

	}

	public void ActivateLayer(int id) {

		foreach (GameObject layer in allLayers) { 
			Collider2D[] colliders = layer.GetComponentsInChildren<Collider2D> ();
			foreach (Collider2D c in colliders) {
				c.enabled = false;
			}
		}

		Collider2D[] activeColliders = allLayers[id].GetComponentsInChildren<Collider2D>();
		foreach(Collider2D c in activeColliders) {
			c.enabled = true;
		}
			
		// find upper collider 
		Vector3 rayPos = new Vector3 (transform.position.x, transform.position.y + 100, transform.position.z); 
		RaycastHit2D hit = Physics2D.Raycast(rayPos, -Vector2.up);

		//Debug.Log (hit.transform.position.y + " : " + transform.position.y);


		if (hit.transform.position.y >= transform.position.y) {
			transform.position = new Vector3 (transform.position.x, hit.transform.position.y + offsetYPos, transform.position.z);  
		}

	}


	public void ActivateUpLayer() {

		currentLayer++;
		if (currentLayer >= allLayers.Length - 1)
			currentLayer = allLayers.Length - 1;
			
		ActivateLayer (currentLayer); 
	}

	public void ActivateDownLayer() {

		currentLayer--;
		if (currentLayer <= 0)
			currentLayer = 0;

		ActivateLayer (currentLayer);
	
	}


}
