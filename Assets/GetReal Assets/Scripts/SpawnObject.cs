﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {

	public Transform prefab;
	public Transform spawnTargetEmpty;
	public Vector2 position;
	public Quaternion rotation;

	public void spawnNewObject () {

		// if spawnTargetEmpty is set get position and rotation from it
		if (spawnTargetEmpty != null) {
			position = spawnTargetEmpty.position;
			rotation = spawnTargetEmpty.rotation;
		} else {
			rotation = Quaternion.identity;
		}

		// spawn new object
		Instantiate (prefab, new Vector3(position.x, position.y, 0), rotation);

	}

}
