﻿using UnityEngine;
using UnityEngine.Events;

public class Event_TriggerEnter2D : MonoBehaviour {

	public string objectTag;
	public UnityEvent triggerEnterEvent;
	public UnityEvent triggerExitEvent;

	void OnTriggerEnter2D( Collider2D other ) {
		if (other.gameObject.tag == objectTag ) {
			triggerEnterEvent.Invoke ();
		}
	}

	void OnTriggerExit2D( Collider2D other ) {
		if (other.gameObject.tag == objectTag ) {
			triggerExitEvent.Invoke ();
		}
	}
}


